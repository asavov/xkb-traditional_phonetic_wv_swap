# xkb-traditional_phonetic_WV_swap

a modification of the stock debian "bg" (bulgarian) keyboard mapping file to swap two letters

The traditional phonetic mapping is nice but (IMO) W=В and V=Ж doesn't make sense.  
This modification swaps them.  
So you get QWERTY -> ЯЖЕРТЪ, and ZXCVB -> ЗѝЦВБ

---
Installation (sort of):
1. go to `/usr/share/X11/xkb/symbols/`
0. make a backup of the `bg` file
0. put this one there
0. `dpkg-reconfigure xkb-data`
0. `dpkg-reconfigure keyboard-configuration`
0. select the "Bulgarian (new phonetic)"
0. reboot